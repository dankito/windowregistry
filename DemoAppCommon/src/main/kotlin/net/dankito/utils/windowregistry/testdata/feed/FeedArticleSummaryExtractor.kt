package net.dankito.utils.windowregistry.testdata.feed

import net.dankito.utils.AsyncResult
import net.dankito.utils.windowregistry.testdata.model.ArticleSummary
import net.dankito.utils.windowregistry.testdata.summary.IArticleSummaryExtractor


class FeedArticleSummaryExtractor(private val feedUrl : String, private val feedReader: IFeedReader) :
    IArticleSummaryExtractor {

    override fun getUrl(): String {
        return feedUrl
    }


    override fun extractSummaryAsync(callback: (AsyncResult<out ArticleSummary>) -> Unit) {
        feedReader.readFeedAsync(feedUrl) {
            callback(it)
        }
    }

    override fun loadMoreItemsAsync(articleSummary: ArticleSummary, callback: (AsyncResult<ArticleSummary>) -> Unit) {
    }
}