package net.dankito.utils.windowregistry.testdata.feed

import net.dankito.utils.AsyncResult
import net.dankito.utils.windowregistry.testdata.model.FeedArticleSummary


interface IFeedReader {

    fun readFeedAsync(feedUrl: String, callback: (AsyncResult<FeedArticleSummary>) -> Unit)

}