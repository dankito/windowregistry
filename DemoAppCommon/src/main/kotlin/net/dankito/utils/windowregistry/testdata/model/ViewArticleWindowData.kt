package net.dankito.utils.windowregistry.testdata.model


class ViewArticleWindowData(var content: String, val baseUri: String, val title: String? = null) {

    private constructor() : this("", "") // for object deserializers

}