package net.dankito.utils.windowregistry.testdata.ui.router

import net.dankito.utils.windowregistry.testdata.model.ViewArticleWindowData
import net.dankito.utils.windowregistry.ui.router.IRouter


interface ITestDataRouter : IRouter {

    fun showArticle(windowData: ViewArticleWindowData)

}