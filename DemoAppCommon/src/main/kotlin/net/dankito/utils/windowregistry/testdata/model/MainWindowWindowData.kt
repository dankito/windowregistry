package net.dankito.utils.windowregistry.testdata.model


class MainWindowWindowData(val articleSummary: ArticleSummary) {

    internal constructor() : this(ArticleSummary()) // for object deserializers

}