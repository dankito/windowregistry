package net.dankito.utils.windowregistry.testdata.model

import java.util.*


data class ArticleSummaryItem(val url : String, var title : String,
                              var summary : String = "", var previewImageUrl : String? = null,
                              var publishedDate: Date? = null, var updatedDate : Date? = null) {

    private constructor() : this("", "") // for Jackson


    override fun toString(): String {
        return "$title: $summary ($url; $previewImageUrl)"
    }

}