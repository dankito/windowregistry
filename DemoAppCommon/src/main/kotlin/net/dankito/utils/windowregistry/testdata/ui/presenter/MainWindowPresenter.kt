package net.dankito.utils.windowregistry.testdata.ui.presenter

import net.dankito.readability4j.Article
import net.dankito.readability4j.extended.Readability4JExtended
import net.dankito.utils.ThreadPool
import net.dankito.utils.web.client.IWebClient
import net.dankito.utils.web.client.RequestParameters
import net.dankito.utils.windowregistry.testdata.feed.FeedArticleSummaryExtractor
import net.dankito.utils.windowregistry.testdata.feed.RomeFeedReader
import net.dankito.utils.windowregistry.testdata.model.ArticleSummary
import net.dankito.utils.windowregistry.testdata.model.ArticleSummaryItem
import net.dankito.utils.windowregistry.testdata.model.ViewArticleWindowData
import net.dankito.utils.windowregistry.testdata.ui.router.ITestDataRouter


class MainWindowPresenter(private val router: ITestDataRouter, private val webClient: IWebClient,
                          private val threadPool: ThreadPool) {


    fun getArticleSummaryTestDataAsync(callback: (ArticleSummary) -> Unit) {
        threadPool.runAsync {
            val feedReader = RomeFeedReader(webClient)
            val articleSummaryExtractor =
                FeedArticleSummaryExtractor("https://www.heise.de/rss/heise-atom.xml", feedReader)

            articleSummaryExtractor.extractSummaryAsync { result ->
                result.result?.let { articleSummary ->
                    callback(articleSummary)
                } ?: callback(ArticleSummary())
            }
        }
    }


    fun getAndShowArticles(articles: MutableCollection<ArticleSummaryItem>) {
        ArrayList(articles).forEach { article ->
            getAndShowArticle(article) { isSuccessful ->
                articles.remove(article)
            }
        }
    }

    fun getAndShowArticle(article: ArticleSummaryItem, callback: ((Boolean) -> Unit)? = null) {
        webClient.getAsync(RequestParameters(article.url)) { response ->
            response.body?.let { responseBody ->
                val readability = Readability4JExtended(article.url, responseBody)
                val parsedArticle = readability.parse()
                showArticle(parsedArticle)
            }

            callback?.invoke(response.isSuccessful)
        }
    }

    private fun showArticle(article: Article) {
        val windowData = ViewArticleWindowData(article.contentWithUtf8Encoding ?: "", article.uri, article.title)

        router.showArticle(windowData)
    }

}