package net.dankito.windowregistry.android

import android.os.Bundle
import kotlinx.android.synthetic.main.activity_view_article.*
import net.dankito.utils.android.ui.activities.ActivityParameterHolder
import net.dankito.utils.windowregistry.android.ui.AndroidWindow
import net.dankito.utils.windowregistry.testdata.model.ViewArticleWindowData
import net.dankito.utils.windowregistry.window.WindowRegistry

class ViewArticleActivity : AndroidWindow() {

    // TODO: retrieve current html from RichTextEditor to save current state


    private val article: ViewArticleWindowData?
        get() = windowData as? ViewArticleWindowData


    override val windowDataClass = ViewArticleWindowData::class.java

    override val displayText: CharSequence
        get() = article?.title ?: super.displayText

    override fun getWindowRegistryInstance(): WindowRegistry {
        return WindowRegistryApplication.windowRegistry
    }

    override fun getParameterHolder(): ActivityParameterHolder {
        return WindowRegistryApplication.parameterHolder
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_view_article)

        article?.let { article ->
            supportActionBar?.title = article.title
            richTextEditor.setHtml(article.content, article.baseUri)
        }

//        val drawerLayout = drawerLayoutOpenActivities
//        drawerLayout.initialize(getWindowRegistry())
    }


    override fun getCurrentWindowData(): Any? {
        return ViewArticleWindowData(richTextEditor.getCachedHtml(), article?.baseUri ?: "", displayText.toString())
    }

}
