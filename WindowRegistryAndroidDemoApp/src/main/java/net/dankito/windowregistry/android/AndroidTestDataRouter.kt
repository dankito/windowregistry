package net.dankito.windowregistry.android

import android.content.Context
import net.dankito.utils.android.ui.activities.ActivityParameterHolder
import net.dankito.utils.windowregistry.android.ui.router.AndroidRouterBase
import net.dankito.utils.windowregistry.testdata.model.ViewArticleWindowData
import net.dankito.utils.windowregistry.testdata.ui.router.ITestDataRouter
import net.dankito.utils.windowregistry.window.WindowRegistry


class AndroidTestDataRouter(applicationContext: Context, windowRegistry: WindowRegistry, parameterHolder: ActivityParameterHolder)
    : AndroidRouterBase(applicationContext, windowRegistry, parameterHolder),
    ITestDataRouter {


    override fun showArticle(windowData: ViewArticleWindowData) {
        showWindow(ViewArticleActivity::class.java, windowData, windowData.title)
    }

}