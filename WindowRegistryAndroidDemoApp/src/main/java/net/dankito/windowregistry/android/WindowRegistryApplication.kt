package net.dankito.windowregistry.android

import android.support.multidex.MultiDexApplication
import net.dankito.utils.android.io.AndroidFileStorageService
import net.dankito.utils.android.ui.activities.ActivityParameterHolder
import net.dankito.utils.serialization.JacksonJsonSerializer
import net.dankito.utils.windowregistry.window.WindowRegistry
import net.dankito.utils.windowregistry.window.WindowStatePersister


class WindowRegistryApplication : MultiDexApplication() {

    companion object {
        lateinit var windowRegistry: WindowRegistry // TODO: find a better way to 'inject' WindowRegistry
            private set

        val parameterHolder = ActivityParameterHolder() // TODO: find a better way to 'inject' WindowRegistry
    }


    init {
        val windowStatePersister = WindowStatePersister(AndroidFileStorageService(this), JacksonJsonSerializer())

        WindowRegistryApplication.windowRegistry = WindowRegistry(windowStatePersister)
    }


}