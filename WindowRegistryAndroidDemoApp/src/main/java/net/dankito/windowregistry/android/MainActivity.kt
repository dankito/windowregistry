package net.dankito.windowregistry.android

import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import net.dankito.utils.ThreadPool
import net.dankito.utils.android.extensions.addHorizontalDividerItemDecoration
import net.dankito.utils.android.ui.activities.ActivityParameterHolder
import net.dankito.utils.web.client.OkHttpWebClient
import net.dankito.utils.windowregistry.android.ui.AndroidMainWindow
import net.dankito.utils.windowregistry.testdata.model.ArticleSummary
import net.dankito.utils.windowregistry.testdata.model.ArticleSummaryItem
import net.dankito.utils.windowregistry.testdata.model.MainWindowWindowData
import net.dankito.utils.windowregistry.testdata.ui.presenter.MainWindowPresenter
import net.dankito.utils.windowregistry.window.WindowRegistry
import net.dankito.windowregistry.android.adapter.ArticleSummaryItemRecyclerAdapter


class MainActivity : AndroidMainWindow() {

    private val router: AndroidTestDataRouter

    private val webclient = OkHttpWebClient()

    private val threadPool = ThreadPool()

    private val presenter: MainWindowPresenter

    private val adapter = ArticleSummaryItemRecyclerAdapter()

    private var lastRetrievedArticleSummary: ArticleSummary? = null


    override val windowDataClass = MainWindowWindowData::class.java


    init {
        router = AndroidTestDataRouter(this, windowRegistry,
            WindowRegistryApplication.parameterHolder)

        presenter = MainWindowPresenter(router, webclient, threadPool)
    }


    override fun getWindowRegistryInstance(): WindowRegistry {
        return WindowRegistryApplication.windowRegistry
    }

    override fun getParameterHolder(): ActivityParameterHolder {
        return WindowRegistryApplication.parameterHolder
    }

    override fun getCurrentWindowData(): Any? {
        lastRetrievedArticleSummary?.let {
            return MainWindowWindowData(it)
        }

        return null
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        initUi()
    }

    private fun initUi() {
        adapter.itemClickListener = { showArticle(it) }

        rcyvwArticles.adapter = adapter
        rcyvwArticles.addHorizontalDividerItemDecoration()

        val lastRetrievedArticleSummary = (windowData as? MainWindowWindowData)?.articleSummary

        if (lastRetrievedArticleSummary != null) {
            this.lastRetrievedArticleSummary = lastRetrievedArticleSummary
            retrievedArticleSummaryOnUiThread(lastRetrievedArticleSummary)
        }
        else {
            presenter.getArticleSummaryTestDataAsync { result ->
                runOnUiThread {
                    retrievedArticleSummaryOnUiThread(result)
                }
            }
        }
    }

    private fun retrievedArticleSummaryOnUiThread(result: ArticleSummary) {
        this.lastRetrievedArticleSummary = result

        adapter.items = result.articles
    }

    private fun showArticle(article: ArticleSummaryItem) {
        presenter.getAndShowArticle(article)
    }

}
