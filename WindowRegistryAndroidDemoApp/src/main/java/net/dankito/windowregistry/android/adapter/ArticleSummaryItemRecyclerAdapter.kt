package net.dankito.windowregistry.android.adapter

import android.view.View
import com.squareup.picasso.Picasso
import net.dankito.utils.android.ui.view.recyclerview.ListRecyclerAdapter
import net.dankito.utils.windowregistry.testdata.model.ArticleSummaryItem
import net.dankito.windowregistry.android.R
import net.dankito.windowregistry.android.adapter.viewholder.ArticleSummaryItemViewHolder


class ArticleSummaryItemRecyclerAdapter : ListRecyclerAdapter<ArticleSummaryItem, ArticleSummaryItemViewHolder>() {

    companion object {

        const val MaxPreviewLength = 400

    }


    override fun getListItemLayoutId() = R.layout.list_item_article_summary_item


    override fun createViewHolder(itemView: View): ArticleSummaryItemViewHolder {
        return ArticleSummaryItemViewHolder(itemView)
    }

    override fun bindItemToView(viewHolder: ArticleSummaryItemViewHolder, item: ArticleSummaryItem) {
        viewHolder.txtTitle.visibility = if(item.title.isBlank()) View.GONE else View.VISIBLE
        viewHolder.txtTitle.text = item.title

        var summary = item.summary
        if(summary.length > MaxPreviewLength) {
            summary = summary.substring(0, MaxPreviewLength) + "..."
        }
        viewHolder.txtSummary.text = summary

        val imageSize = viewHolder.itemView.context.resources
            .getDimensionPixelSize(R.dimen.list_item_article_summary_preview_image_size)

        Picasso.with(viewHolder.itemView.context)
            .load(item.previewImageUrl)
            .resize(imageSize, 0)
            .into(viewHolder.imgPreviewImage)
    }

}