package net.dankito.utils.windowregistry.javafx.ui.router

import net.dankito.utils.windowregistry.javafx.ui.window.article.ViewArticleWindow
import net.dankito.utils.windowregistry.testdata.model.ViewArticleWindowData
import net.dankito.utils.windowregistry.testdata.ui.router.ITestDataRouter
import net.dankito.utils.windowregistry.window.javafx.ui.router.JavaFXRouterBase
import tornadofx.Component


class JavaFXTestDataRouter(component: Component) : JavaFXRouterBase(component),
    ITestDataRouter {


    override fun showArticle(windowData: ViewArticleWindowData) {
        showWindow(ViewArticleWindow::class.java, windowData, windowData.title)
    }

}