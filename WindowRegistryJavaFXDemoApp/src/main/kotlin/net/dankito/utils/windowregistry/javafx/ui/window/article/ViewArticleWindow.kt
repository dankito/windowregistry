package net.dankito.utils.windowregistry.javafx.ui.window.article

import javafx.scene.layout.Priority
import javafx.scene.layout.VBox
import net.dankito.richtexteditor.java.fx.RichTextEditor
import net.dankito.utils.windowregistry.javafx.ui.window.main.MainWindow
import net.dankito.utils.windowregistry.testdata.model.ViewArticleWindowData
import net.dankito.utils.windowregistry.window.WindowRegistry
import net.dankito.utils.windowregistry.window.javafx.ui.JavaFXWindow
import tornadofx.useMaxHeight
import tornadofx.useMaxWidth
import tornadofx.vbox


class ViewArticleWindow : JavaFXWindow() {


    private val article: ViewArticleWindowData?
        get() = windowData as? ViewArticleWindowData


    private lateinit var editor: RichTextEditor


    override val windowDataClass = ViewArticleWindowData::class.java


    override fun getWindowRegistryInstance(): WindowRegistry {
        return MainWindow.WindowRegistry
    }


    override val root = vbox {
        prefWidth = 905.0
        prefHeight = 650.0

        this@ViewArticleWindow.editor = initRichTextEditor()
        add(editor)
    }

    private fun initRichTextEditor(): RichTextEditor {
        val editor = RichTextEditor()

        editor.useMaxWidth = true
        editor.useMaxHeight = true

        VBox.setVgrow(editor, Priority.ALWAYS)

        article?.let { article ->
            editor.setHtml(article.content, article.baseUri)
        }

        return editor
    }


    override fun getCurrentWindowData(): Any? {
        return ViewArticleWindowData(editor.getCurrentHtmlBlocking(), article?.baseUri ?: "", article?.title)
    }

}