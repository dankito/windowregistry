package net.dankito.utils.windowregistry.javafx.ui.window.main.controls

import javafx.beans.property.SimpleStringProperty
import javafx.collections.FXCollections
import javafx.collections.ObservableSet
import javafx.scene.layout.Priority
import net.dankito.utils.windowregistry.javafx.ui.window.main.model.ArticleSummaryItemViewModel
import net.dankito.utils.windowregistry.testdata.model.ArticleSummary
import net.dankito.utils.windowregistry.testdata.model.ArticleSummaryItem
import net.dankito.utils.windowregistry.testdata.model.MainWindowWindowData
import net.dankito.utils.windowregistry.testdata.ui.presenter.MainWindowPresenter
import tornadofx.*
import java.text.DateFormat


class ArticleSummaryView(private val presenter: MainWindowPresenter, private val windowData: MainWindowWindowData?) : View() {

    companion object {
        private const val TestDataLabelTopAndBottomMargin = 12.0

        private const val TestDataButtonsWidth = 130.0
        private const val TestDataButtonsTopAndBottomMargin = 4.0

        private val ArticleSummaryInfoDateTimeFormat = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM)
    }



    private val articleSummaryItems = FXCollections.observableArrayList<ArticleSummaryItem>()

    private val selectedArticleSummaryItems: ObservableSet<ArticleSummaryItem> = FXCollections.observableSet(LinkedHashSet())

    private val articleSummaryItemsInfo = SimpleStringProperty("")

    private val articleSummaryItemModel = ArticleSummaryItemViewModel()


    private var lastRetrievedArticleSummary: ArticleSummary? = null


    override val root = vbox {

        listview<ArticleSummaryItem>(articleSummaryItems) {
            userData =
                selectedArticleSummaryItems // bad code design, but found no other way to pass selectedArticleSummaryItems on ArticleSummaryItemListCellFragment

            cellFragment(ArticleSummaryItemListCellFragment::class)

            bindSelected(articleSummaryItemModel)

            onDoubleClick {
                selectedItem?.let { presenter.getAndShowArticle(it) }
            }

            vboxConstraints {
                vGrow = Priority.ALWAYS
            }
        }

        anchorpane {
            prefHeight = 30.0

            label(articleSummaryItemsInfo) {
                anchorpaneConstraints {
                    leftAnchor = 0.0
                    topAnchor = TestDataLabelTopAndBottomMargin
                    bottomAnchor = TestDataLabelTopAndBottomMargin
                }
            }

            button(messages["show.articles"]) {
                prefWidth = TestDataButtonsWidth

                action { presenter.getAndShowArticles(selectedArticleSummaryItems) }

                anchorpaneConstraints {
                    rightAnchor = TestDataButtonsWidth + 12.0
                    topAnchor = TestDataButtonsTopAndBottomMargin
                    bottomAnchor = TestDataButtonsTopAndBottomMargin
                }
            }

            button(messages["update.test.data"]) {
                prefWidth = TestDataButtonsWidth

                action { loadTestData() }

                anchorpaneConstraints {
                    rightAnchor = 0.0
                    topAnchor = TestDataButtonsTopAndBottomMargin
                    bottomAnchor = TestDataButtonsTopAndBottomMargin
                }
            }
        }

        restoreOrLoadTestData()
    }


    private fun restoreOrLoadTestData() {
        if (windowData != null) {
            retrievedArticleSummaryItemsOnUiThread(windowData.articleSummary)
        }
        else {
            loadTestData()
        }
    }

    private fun loadTestData() {
        presenter.getArticleSummaryTestDataAsync { articleSummary ->
            runLater {
                retrievedArticleSummaryItemsOnUiThread(articleSummary)
            }
        }
    }

    private fun retrievedArticleSummaryItemsOnUiThread(articleSummary: ArticleSummary) {
        this.lastRetrievedArticleSummary = articleSummary

        articleSummaryItems.setAll(articleSummary.articles)

        articleSummaryItemsInfo.set(
            String.format(
                messages["article.summary.info"],
                articleSummary.articles.size,
                ArticleSummaryInfoDateTimeFormat.format(articleSummary.time)
            )
        )
    }


    fun getCurrentState(): MainWindowWindowData? {
        lastRetrievedArticleSummary?.let {
            return MainWindowWindowData(it)
        }

        return null
    }

}