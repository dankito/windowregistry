package net.dankito.utils.windowregistry.javafx.ui.window.main.model

import javafx.beans.property.SimpleStringProperty
import net.dankito.utils.windowregistry.testdata.model.ArticleSummaryItem
import tornadofx.ItemViewModel


class ArticleSummaryItemViewModel : ItemViewModel<ArticleSummaryItem>() {

    val previewImageUrl = bind { SimpleStringProperty(item?.previewImageUrl) }

    val title = bind { SimpleStringProperty(item?.title) }

    val summary = bind { SimpleStringProperty(item?.summary ?: "") }

}