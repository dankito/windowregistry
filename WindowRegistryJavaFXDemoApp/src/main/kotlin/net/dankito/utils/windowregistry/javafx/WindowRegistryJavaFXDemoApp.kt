package net.dankito.utils.windowregistry.javafx

import javafx.application.Application
import javafx.stage.Stage
import net.dankito.utils.localization.UTF8ResourceBundleControl
import net.dankito.utils.windowregistry.javafx.ui.window.main.MainWindow
import tornadofx.App
import tornadofx.FX
import java.util.*


class WindowRegistryJavaFXDemoApp : App(MainWindow::class) {

    override fun start(stage: Stage) {
        setupMessagesResources() // has to be done before creating / injecting first instances as some of them already rely on Messages

        super.start(stage)
    }


    private fun setupMessagesResources() {
        ResourceBundle.clearCache() // at this point default ResourceBundles are already created and cached. In order that ResourceBundle created below takes effect cache has to be cleared before

        FX.messages = ResourceBundle.getBundle("WindowRegistry_Messages", UTF8ResourceBundleControl())
    }

    @Throws(Exception::class)
    override fun stop() {
        super.stop()
        System.exit(0) // otherwise Window would be closed but application still running in background
    }

}



fun main(args: Array<String>) {
    Application.launch(WindowRegistryJavaFXDemoApp::class.java, *args)
}