package net.dankito.utils.windowregistry.javafx.ui.window.main

import javafx.application.Platform
import javafx.scene.layout.Priority
import net.dankito.utils.ThreadPool
import net.dankito.utils.io.JavaFileStorageService
import net.dankito.utils.serialization.JacksonJsonSerializer
import net.dankito.utils.web.client.IWebClient
import net.dankito.utils.web.client.OkHttpWebClient
import net.dankito.utils.windowregistry.javafx.ui.router.JavaFXTestDataRouter
import net.dankito.utils.windowregistry.javafx.ui.window.main.controls.ArticleSummaryView
import net.dankito.utils.windowregistry.testdata.model.MainWindowWindowData
import net.dankito.utils.windowregistry.testdata.ui.presenter.MainWindowPresenter
import net.dankito.utils.windowregistry.window.WindowRegistry
import net.dankito.utils.windowregistry.window.WindowStatePersister
import net.dankito.utils.windowregistry.window.javafx.ui.JavaFXMainWindow
import net.dankito.utils.windowregistry.window.javafx.ui.controls.OpenedWindowsView
import tornadofx.*


class MainWindow : JavaFXMainWindow(FX.messages["application.title"]) {

    companion object {

        lateinit var WindowRegistry: WindowRegistry
            private set

    }


    private val router = JavaFXTestDataRouter(this)

    private val webClient: IWebClient = OkHttpWebClient()

    private val threadPool = ThreadPool()

    private val presenter = MainWindowPresenter(router, webClient, threadPool)


    private var articleSummaryView: ArticleSummaryView by singleAssign()


    override val windowDataClass = MainWindowWindowData::class.java


    override fun getWindowRegistryInstance(): WindowRegistry {
        val windowStatePersister = WindowStatePersister(JavaFileStorageService(), JacksonJsonSerializer())

        return WindowRegistry(windowStatePersister)
    }


    init {
        WindowRegistry = windowRegistry
    }


    override val root = vbox {
        prefWidth = 850.0
        prefHeight = 450.0

        splitpane {
            vboxConstraints {
                vgrow = Priority.ALWAYS
            }

            add(OpenedWindowsView(windowRegistry))

            articleSummaryView = ArticleSummaryView(presenter, windowData as? MainWindowWindowData)
            add(articleSummaryView)

            this.setDividerPosition(0, 0.25)
        }
    }


    override fun getCurrentWindowData(): Any? {
        return articleSummaryView.getCurrentState()
    }


    override fun onUndock() {
        super.onUndock()

        shutdownApplication()
    }

    private fun shutdownApplication() {
        Platform.exit() // stop application as otherwise all other windows would stay open
    }

}