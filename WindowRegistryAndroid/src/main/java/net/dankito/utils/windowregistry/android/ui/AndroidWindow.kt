package net.dankito.utils.windowregistry.android.ui

import android.graphics.Bitmap
import android.os.Bundle
import android.view.View
import net.dankito.utils.android.io.AndroidFileStorageService
import net.dankito.utils.android.ui.activities.ActivityParameterHolder
import net.dankito.utils.android.ui.activities.ThemeableActivity
import net.dankito.utils.serialization.JacksonJsonSerializer
import net.dankito.utils.ui.image.Image
import net.dankito.utils.windowregistry.android.ui.router.AndroidRouterBase
import net.dankito.utils.windowregistry.window.IWindow
import net.dankito.utils.windowregistry.window.WindowListener
import net.dankito.utils.windowregistry.window.WindowRegistry
import net.dankito.utils.windowregistry.window.WindowStatePersister
import org.slf4j.LoggerFactory
import java.io.File
import java.io.FileOutputStream
import java.util.*


abstract class AndroidWindow : ThemeableActivity(), IWindow {

    companion object {
        const val WindowStateIdIntentExtraName = "WINDOW_STATE_ID"

        const val WindowDataParameterIdIntentExtraName = "WINDOW_DATA_PARAMETER_ID"

        const val WindowDataFilePathIntentExtraName = "WINDOW_DATA_FILE_PATH"

        private val log = LoggerFactory.getLogger(AndroidWindow::class.java)
    }


    /**
     * Called right before [getWindowRegistryInstance()].
     *
     * May is needed in child classes so they can set up their dependency injection to return WindowRegistry in [getWindowRegistryInstance()].
     */
    protected open fun setupDependencyInjection() { }

    /**
     * Create the application wide WindowRegistry or return the one injected in [setupDependencyInjection].
     *
     * Make sure that for the whole application there's only one WindowRegistry instance.
     *
     * This method is called right in constructor of JavaFXWindow - so your derived class is not
     * initialized yet! - and just after [setupDependencyInjection].
     */
    protected abstract fun getWindowRegistryInstance(): WindowRegistry


    protected abstract fun getParameterHolder(): ActivityParameterHolder


    protected val windowRegistry: WindowRegistry

    override var windowData: Any? = null

    override val displayText: CharSequence
        get() = supportActionBar?.title ?: this.title ?: javaClass.simpleName

    override val windowIcon: Image? = null

    override val isMainWindow = false


    override val screenX = 0.0

    override val screenY = 0.0

    override val width = 0.0

    override val height = 0.0


    protected var hasSaveInstanceStateBeenCalled = false

    protected val listeners = LinkedHashSet<WindowListener>()


    init {
        setupDependencyInjection()

        windowRegistry = getWindowRegistryInstance()
    }

    protected open fun initMainWindow() {

        windowRegistry.restoreWindowStates(this, AndroidRouterBase(this, windowRegistry, getParameterHolder()))?.let { windowState ->
            this.windowData = windowState.windowData
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        log.info("onCreate() called for $this")

        this.windowData = getWindowDataToRestore(savedInstanceState)

        if (this.isMainWindow) {
            initMainWindow()
        }

        val windowStateId = intent?.getStringExtra(WindowStateIdIntentExtraName)

        val windowState = windowRegistry.windowCreated(this, windowData, windowStateId)

        if (windowData == null && windowState != null) {
            windowData = windowState.windowData
        }
    }

    override fun onStart() {
        super.onStart()

        log.info("onStart() called for $this")
    }

    override fun onResume() {
        super.onResume()

        log.info("onResume() called for $this")

        callWindowCameToForegroundListeners()
    }

    override fun onPause() {
        log.info("onPause() called for $this")

        if (isFinishing == false) { // only call listener if Activity doesn't get destroyed as only then it's senseful to save window data
            callWindowGoesToBackgroundListeners()
        }

        super.onPause()
    }

    override fun onStop() {
        log.info("onStop() called for $this")

        super.onStop()
    }

    override fun onDestroy() {
        log.info("onDestroy() called for $this")

        // do not clear parameters if window is going to be re-created soon from savedInstanceState
        if (hasSaveInstanceStateBeenCalled == false) {
            getParametersId()?.let { parametersId ->
                getParameterHolder().clearParameters(parametersId)
            }
        }

        callWindowClosesListeners()

        super.onDestroy()
    }



    protected open fun getWindowDataToRestore(savedInstanceState: Bundle?): Any? {
        val parametersId = getParametersId()
            ?: savedInstanceState?.getString(WindowDataParameterIdIntentExtraName, null)

        parametersId?.let { parameterId ->
            getParameterHolder().getParameters(parameterId)?.let { windowData ->
                return windowData
            }
        }

        intent?.let { intent ->
            intent.getStringExtra(WindowDataFilePathIntentExtraName)?.let { windowDataFilePath ->
                getWindowDataToRestoreFromFile(windowDataFilePath)?.let { windowData ->
                    return windowData
                }
            }
        }

        return null
    }

    protected open fun getParametersId(): String? {
        return intent?.getStringExtra(WindowDataParameterIdIntentExtraName)
    }

    protected open fun getWindowDataToRestoreFromFile(windowDataFilePath: String): Any? {
        windowDataClass?.let { windowDataClass ->
            val windowStatePersister =
                WindowStatePersister(AndroidFileStorageService(this), JacksonJsonSerializer()) // TODO: inject

            try {
                windowStatePersister.restoreWindowData(windowDataClass, File(windowDataFilePath))?.let { windowData ->
                    return windowData
                }
            } catch (e: Exception) {
                log.error("could not restore window data for class $windowDataClass from file $windowDataFilePath")
            }
        }

        return null
    }


    override fun onSaveInstanceState(outState: Bundle?) {
        hasSaveInstanceStateBeenCalled = true

        log.info("onSaveInstanceState() called for $this")

        super.onSaveInstanceState(outState)

        outState?.let {
            outState.putString(WindowDataParameterIdIntentExtraName, getParametersId())
        }
    }


    override fun saveScreenshotTo(screenshotFile: File): File? {
        val rootView = window.decorView.findViewById<View>(android.R.id.content)

        val screenShot = getScreenShot(rootView)

        store(screenShot, screenshotFile)

        return screenshotFile
    }

    protected open fun getScreenShot(view: View): Bitmap {
        val screenView = view.rootView

        screenView.isDrawingCacheEnabled = true
        val bitmap = Bitmap.createBitmap(screenView.drawingCache)
        screenView.isDrawingCacheEnabled = false

        return bitmap
    }

    protected open fun store(bitmap: Bitmap, file: File) {
        try {
            val outputStream = FileOutputStream(file)
            bitmap.compress(Bitmap.CompressFormat.PNG, 85, outputStream)
            outputStream.flush()
            outputStream.close()
        } catch (e: Exception) {
            log.error("Could not save screenshot", e)
        }

    }


    override fun addWindowListener(listener: WindowListener) {
        listeners.add(listener)
    }

    override fun removeWindowListener(listener: WindowListener) {
        listeners.remove(listener)
    }

    protected open fun callWindowCameToForegroundListeners() {
        ArrayList(listeners).forEach { listener ->
            listener.windowCameToForeground()
        }
    }

    protected open fun callWindowGoesToBackgroundListeners() {
        ArrayList(listeners).forEach { listener ->
            listener.windowGoesToBackground()
        }
    }

    protected open fun callWindowClosesListeners() {
        ArrayList(listeners).forEach { listener ->
            listener.windowCloses()
        }
    }


    override fun toString(): String {
        return displayText.toString()
    }

}