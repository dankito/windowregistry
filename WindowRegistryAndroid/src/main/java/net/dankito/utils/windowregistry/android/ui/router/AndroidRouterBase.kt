package net.dankito.utils.windowregistry.android.ui.router

import android.content.Context
import android.content.Intent
import net.dankito.utils.android.ui.activities.ActivityParameterHolder
import net.dankito.utils.image.ImageReference
import net.dankito.utils.windowregistry.android.ui.AndroidWindow
import net.dankito.utils.windowregistry.ui.router.IRouter
import net.dankito.utils.windowregistry.window.WindowRegistry


open class AndroidRouterBase(protected val applicationContext: Context, protected val windowRegistry: WindowRegistry,
                             protected val parameterHolder: ActivityParameterHolder) : IRouter {


    /**
     * Should only be used from WindowRegistry
     */
    override fun showWindow(windowClass: Class<*>, windowStateId: String?) {
        if (windowStateId == null) {
            startActivity(windowClass)
        }
        else {
            startActivity(windowClass, Pair(AndroidWindow.WindowStateIdIntentExtraName, windowStateId))
        }
    }

    open fun showWindow(windowClass: Class<*>, windowData: Any? = null, title: String? = null, icon: ImageReference? = null) {
        showWindow(windowClass, windowData?.javaClass, windowData, title, icon)
    }

    override fun showWindow(windowClass: Class<*>, windowDataClass: Class<*>?, windowData: Any?, title: String?, icon: ImageReference?) { // icon is not used on Android
        // as not on all calls to Context.startActivity() an Activity really gets displayed, e.g. if there are
        // to many Activities display, we inform WindowRegistry about its existence so that it can keep track of
        val windowStateId = windowRegistry.goingToCreateNewWindow(windowClass, windowDataClass, windowData, title)

        if (windowData == null) {
            startActivity(windowClass,
                Pair(AndroidWindow.WindowStateIdIntentExtraName, windowStateId))
        }
        else {
            val parameterId = parameterHolder.setParameters(windowData)

            startActivity(windowClass,
                Pair(AndroidWindow.WindowStateIdIntentExtraName, windowStateId),
                Pair(AndroidWindow.WindowDataParameterIdIntentExtraName, parameterId))
        }
    }

    open fun startActivity(windowClass: Class<*>, vararg parameters: Pair<String, String>) {
        val intent = Intent(applicationContext, windowClass)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

        parameters.forEach { parameter ->
            intent.putExtra(parameter.first, parameter.second)
        }

        applicationContext.startActivity(intent)
    }

}