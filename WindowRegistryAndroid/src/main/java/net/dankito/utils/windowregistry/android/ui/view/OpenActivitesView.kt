package net.dankito.utils.windowregistry.android.ui.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.RelativeLayout
import kotlinx.android.synthetic.main.view_open_activities.view.*
import net.dankito.utils.android.extensions.addHorizontalDividerItemDecoration
import net.dankito.utils.android.extensions.asActivity
import net.dankito.utils.windowregistry.android.R
import net.dankito.utils.windowregistry.android.ui.adapter.OpenActivitiesAdapter
import net.dankito.utils.windowregistry.window.WindowRegistry
import net.dankito.utils.windowregistry.window.WindowState
import org.slf4j.LoggerFactory


open class OpenActivitesView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : RelativeLayout(context, attrs, defStyleAttr) {

    companion object {
        private val log = LoggerFactory.getLogger(OpenActivitesView::class.java)
    }


    private val openActivitiesAdapter = OpenActivitiesAdapter()

    private lateinit var windowRegistry: WindowRegistry


    init {
        setupUi()
    }

    protected open fun setupUi() {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val rootView = inflater.inflate(R.layout.view_open_activities, this)

        val rcyvwOpenActivities = rootView.rcyvwOpenActivities

        rcyvwOpenActivities.addHorizontalDividerItemDecoration()
        rcyvwOpenActivities.adapter = openActivitiesAdapter

        openActivitiesAdapter.itemClickListener = { item -> openActivityClicked(item) }
    }


    open fun initialize(windowRegistry: WindowRegistry) {
        this.windowRegistry = windowRegistry

        updateOpenActivities()
    }

    open fun showOpenActivities() {
        updateOpenActivities()
    }

    protected open fun updateOpenActivities() {
        context.asActivity()?.runOnUiThread {
            updateOpenActivitiesOnUiThread()
        }
    }

    protected open fun updateOpenActivitiesOnUiThread() {
        openActivitiesAdapter.items = windowRegistry.windowStates
    }


    private fun openActivityClicked(clickedActivityState: WindowState) {
        try {
            val clickedActivityClass = Class.forName(clickedActivityState.windowClass)

            // TODO: implement navigating to Activity
//            windowRegistry.router.showWindow(clickedActivityClass, clickedActivityState)
        } catch (e: Exception) {
            log.error("Could not navigate to clicked activity $clickedActivityState", e)
        }
    }


}