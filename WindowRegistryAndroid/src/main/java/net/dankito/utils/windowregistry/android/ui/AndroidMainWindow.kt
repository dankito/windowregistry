package net.dankito.utils.windowregistry.android.ui


abstract class AndroidMainWindow : AndroidWindow() {

    override val isMainWindow = true

}