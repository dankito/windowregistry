package net.dankito.utils.windowregistry.android.ui.view

import android.content.Context
import android.support.v4.widget.DrawerLayout
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import kotlinx.android.synthetic.main.drawer_layout_open_activities.view.*
import net.dankito.utils.windowregistry.android.R
import net.dankito.utils.windowregistry.window.WindowRegistry


open class OpenActivitiesDrawerLayout @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : DrawerLayout(context, attrs, defStyleAttr) {

    protected lateinit var openActivitiesView: OpenActivitesView


    init {
        setupUi()
    }

    private fun setupUi() {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val rootView = inflater.inflate(R.layout.drawer_layout_open_activities, this)

        openActivitiesView = rootView.viewOpenActivities

        this.addDrawerListener(object : DrawerLayout.SimpleDrawerListener() {
            override fun onDrawerStateChanged(newState: Int) {
            }

            override fun onDrawerSlide(drawerView: View, slideOffset: Float) {
            }

            override fun onDrawerClosed(drawerView: View) {
            }

            override fun onDrawerOpened(drawerView: View) {
                openActivitiesView.showOpenActivities()
            }

        })
    }


    open fun initialize(windowRegistry: WindowRegistry) {
        openActivitiesView.initialize(windowRegistry)
    }

}