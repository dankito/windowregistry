package net.dankito.utils.windowregistry.android.ui.adapter.viewholder

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import kotlinx.android.synthetic.main.list_item_open_activity.view.*


class OpenActivitiesViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    val imgvwOpenWindowScreenshot: ImageView = itemView.imgvwOpenWindowScreenshot

    val imgvwOpenWindowIcon: ImageView = itemView.imgvwOpenWindowIcon

    val txtActivityTitle: TextView = itemView.txtOpenActivityTitle

}