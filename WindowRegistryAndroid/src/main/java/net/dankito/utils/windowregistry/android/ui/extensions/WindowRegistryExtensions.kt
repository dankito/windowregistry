package net.dankito.utils.windowregistry.android.ui.extensions

import android.app.Activity
import net.dankito.utils.windowregistry.android.ui.AndroidWindow
import net.dankito.utils.windowregistry.window.WindowRegistry


val WindowRegistry.currentActivity: Activity?
    get() = this.currentWindow.asActivity()

val WindowRegistry.currentAndroidWindow: AndroidWindow?
    get() = this.currentWindow.asAndroidWindow()