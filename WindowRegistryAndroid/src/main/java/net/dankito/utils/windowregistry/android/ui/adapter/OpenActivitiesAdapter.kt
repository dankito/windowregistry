package net.dankito.utils.windowregistry.android.ui.adapter

import android.net.Uri
import android.view.View
import net.dankito.utils.android.image.AndroidImage
import net.dankito.utils.android.ui.view.recyclerview.ListRecyclerAdapter
import net.dankito.utils.windowregistry.android.R
import net.dankito.utils.windowregistry.android.ui.adapter.viewholder.OpenActivitiesViewHolder
import net.dankito.utils.windowregistry.window.WindowState


class OpenActivitiesAdapter : ListRecyclerAdapter<WindowState, OpenActivitiesViewHolder>() {

    override fun getListItemLayoutId() = R.layout.list_item_open_activity


    override fun createViewHolder(itemView: View): OpenActivitiesViewHolder {
        return OpenActivitiesViewHolder(itemView)
    }

    override fun bindItemToView(viewHolder: OpenActivitiesViewHolder, item: WindowState) {
        viewHolder.imgvwOpenWindowScreenshot.visibility = if (item.screenshotFile != null) View.VISIBLE else View.INVISIBLE
        item.screenshotFile?.let { screenshotFile ->
            viewHolder.imgvwOpenWindowScreenshot.setImageURI(Uri.fromFile(screenshotFile))
        }

        viewHolder.imgvwOpenWindowIcon.visibility = if (item.icon != null) View.VISIBLE else View.INVISIBLE
        (item.icon as? AndroidImage)?.let { icon ->
            viewHolder.imgvwOpenWindowIcon.setImageDrawable(icon.drawable)
        }

        viewHolder.txtActivityTitle.text = item.title
    }

}