package net.dankito.utils.windowregistry.android.ui.extensions

import android.app.Activity
import net.dankito.utils.windowregistry.android.ui.AndroidWindow
import net.dankito.utils.windowregistry.window.IWindow


fun IWindow?.asActivity(): Activity? {
    return this as? Activity
}

fun IWindow?.asAndroidWindow(): AndroidWindow? {
    return this as? AndroidWindow
}