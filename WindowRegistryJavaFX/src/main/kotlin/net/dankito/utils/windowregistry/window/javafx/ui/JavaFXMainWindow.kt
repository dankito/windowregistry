package net.dankito.utils.windowregistry.window.javafx.ui


abstract class JavaFXMainWindow(title: String? = null) : JavaFXWindow(title) {


    override val isMainWindow = true



    init {
        initMainWindow()
    }


    override fun onUndock() {
        saveWindows()

        super.onUndock()
    }


    protected open fun saveWindows() {
        windowRegistry.applicationCloses()
    }

}