package net.dankito.utils.windowregistry.window.javafx.ui

import javafx.embed.swing.SwingFXUtils
import javafx.scene.Scene
import javafx.scene.image.Image
import javafx.stage.Modality
import javafx.stage.Stage
import javafx.stage.StageStyle
import javafx.stage.Window
import net.dankito.utils.javafx.ui.image.JavaFXImage
import net.dankito.utils.windowregistry.window.IWindow
import net.dankito.utils.windowregistry.window.WindowListener
import net.dankito.utils.windowregistry.window.WindowRegistry
import net.dankito.utils.windowregistry.window.WindowState
import net.dankito.utils.windowregistry.window.javafx.ui.router.JavaFXRouterBase
import tornadofx.Fragment
import tornadofx.onChangeOnce
import tornadofx.runLater
import java.io.File
import javax.imageio.ImageIO


abstract class JavaFXWindow(title: String? = null) : Fragment(title), IWindow {

    companion object {

        private val WindowDataNullObject = Any()

        private const val WindowStateIdNullObject = ""

    }


    /**
     * Called right before [getWindowRegistryInstance()].
     *
     * May is needed in child classes so they can set up their dependency injection to return WindowRegistry in [getWindowRegistryInstance()].
     */
    protected open fun setupDependencyInjection() { }

    /**
     * Create the application wide WindowRegistry or return the one injected in [setupDependencyInjection].
     *
     * Make sure that for the whole application there's only one WindowRegistry instance.
     *
     * This method is called right in constructor of JavaFXWindow - so your derived class is not
     * initialized yet! - and just after [setupDependencyInjection].
     */
    protected abstract fun getWindowRegistryInstance(): WindowRegistry


    protected open val windowRegistry: WindowRegistry

    protected lateinit var stage: Stage

    protected val listeners = LinkedHashSet<WindowListener>()


    fun show(title: String? = null, iconUrl: String? = null, stageStyle: StageStyle = StageStyle.DECORATED,
             modality: Modality = Modality.NONE, owner: Window? = null) : Stage {
        val dialogStage = Stage()

        dialogStage.title = title ?: ""
        iconUrl?.let { setIcon(dialogStage, it) }
        owner?.let { dialogStage.initOwner(it) }

        dialogStage.initModality(modality)
        dialogStage.initStyle(stageStyle)

        val scene = Scene(this.root)

        dialogStage.scene = scene

        dialogStage.show()
        dialogStage.requestFocus()
        dialogStage.toFront()

        this.modalStage = dialogStage

        return dialogStage
    }

    protected open fun setIcon(dialogStage: Stage, iconUrl: String) {
        // TODO: use ImageCache
        dialogStage.icons.add(Image(iconUrl))
    }

    protected open fun setStageSizeAndPosition(state: WindowState, stage: Stage) {
        if (state.isWindowPositionSet) {
            stage.x = state.screenX
            stage.y = state.screenY
        }

        if (state.isWindowSizeSet) {
            stage.width = state.width
            stage.height = state.height
        }
    }


    val windowDataParam: Any by param(WindowDataNullObject) // by param() doesn't seem to like when passing null - on calling get() an exception gets thrown

    val windowStateIdParam: String by param(WindowStateIdNullObject) // by param() doesn't seem to like when passing null - on calling get() an exception gets thrown

    protected var windowStateId: String? = null


    override var windowData: Any? = null

    override val displayText: CharSequence
        get() = stage.title ?: javaClass.simpleName

    override val windowIcon: net.dankito.utils.ui.image.Image?
        get() {
            if (stage.icons.isNotEmpty()) {
                return JavaFXImage(stage.icons[0])
            }

            return null
        }

    override val isMainWindow = false


    override val screenX: Double
        get() = stage.x

    override val screenY: Double
        get() = stage.y

    override val width: Double
        get() = stage.width

    override val height: Double
        get() = stage.height


    init {
        setupDependencyInjection()

        windowRegistry = getWindowRegistryInstance()

        windowData = if(windowDataParam == WindowDataNullObject) null else windowDataParam

        windowStateId = if (windowStateIdParam == WindowStateIdNullObject) null else windowStateIdParam

        if (windowData == null && windowStateId != null) {
            windowData = windowRegistry.getWindowDataForStateId(windowStateId)
        }
    }


    protected open fun initMainWindow() {
        this.stage = this.primaryStage

        windowRegistry.restoreWindowStates(this, JavaFXRouterBase(this))?.let { windowState ->
            this.windowData = windowState.windowData // TODO: still needed?
            this.windowStateId = windowState.id
        }
    }


    override fun onDock() {
        super.onDock()

        if (this::stage.isInitialized ) {
            initForStage(this.stage)
        }
        else {
            (root.scene.window as? Stage)?.let {
                initForStage(it)
            }
            ?: root.scene.windowProperty().onChangeOnce { window ->
                (window as? Stage)?.let {
                    runLater { // run later to ensure stage is already displayed otherwise width and height gets set to wrong values
                        initForStage(it)
                    }
                }
            }
        }
    }

    override fun onUndock() {
        callWindowClosesListeners()

        super.onUndock()
    }

    protected open fun initForStage(stage: Stage) {
        this.stage = stage

        val windowState = windowRegistry.windowCreated(this, windowData, windowStateId)

        trackWindowStateForListeners(this.stage)

        restoreWindowState(windowState, this.stage)
    }


    override fun saveScreenshotTo(screenshotFile: File): File? {
        val screenshot = this.stage.scene.snapshot(null)

        ImageIO.write(SwingFXUtils.fromFXImage(screenshot, null), "png", screenshotFile)

        return screenshotFile
    }


    protected open fun restoreWindowState(state: WindowState, stage: Stage) {
        if (stage.titleProperty().isBound == false) {
            stage.title = state.title
        }

        // TODO: restore icon

        setStageSizeAndPosition(state, stage)
    }


    override fun addWindowListener(listener: WindowListener) {
        listeners.add(listener)
    }

    override fun removeWindowListener(listener: WindowListener) {
        listeners.remove(listener)
    }

    protected open fun trackWindowStateForListeners(dialogStage: Stage) {
        dialogStage.focusedProperty().addListener { _, _, newValue ->
            if (newValue) {
                callWindowCameToForegroundListeners()
            }
            else {
                callWindowGoesToBackgroundListeners()
            }
        }
    }

    protected open fun callWindowCameToForegroundListeners() {
        ArrayList(listeners).forEach { listener ->
            listener.windowCameToForeground()
        }
    }

    protected open fun callWindowGoesToBackgroundListeners() {
        ArrayList(listeners).forEach { listener ->
            listener.windowGoesToBackground()
        }
    }

    protected open fun callWindowClosesListeners() {
        ArrayList(listeners).forEach { listener ->
            listener.windowCloses()
        }
    }


    override fun toString(): String {
        return displayText.toString()
    }

}