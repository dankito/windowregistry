package net.dankito.utils.windowregistry.window.javafx.ui.model

import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import javafx.scene.image.Image
import net.dankito.utils.javafx.ui.image.JavaFXImage
import net.dankito.utils.windowregistry.window.WindowState
import tornadofx.ItemViewModel


open class OpenWindowItemViewModel : ItemViewModel<WindowState>() {

    open val screenshotUrl = bind { SimpleStringProperty(item?.screenshotFile?.toURI()?.toASCIIString()) }

    open val icon = bind { SimpleObjectProperty<Image>((item?.icon as? JavaFXImage)?.image) }

    open val title = bind { SimpleStringProperty(item?.title) }

}