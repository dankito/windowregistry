package net.dankito.utils.windowregistry.window.javafx.ui.router

import net.dankito.utils.image.ImageReference
import net.dankito.utils.javafx.ui.image.JavaFXImageReference
import net.dankito.utils.windowregistry.ui.router.IRouter
import net.dankito.utils.windowregistry.window.javafx.ui.JavaFXWindow
import tornadofx.Component
import tornadofx.runLater


open class JavaFXRouterBase(protected val component: Component) : IRouter {


    override fun showWindow(windowClass: Class<*>, windowStateId: String?) {
        runLater {
            val window = component.find(windowClass as Class<out JavaFXWindow>,
                mapOf(JavaFXWindow::windowStateIdParam to windowStateId))

            window.show()
        }
    }

    open fun showWindow(windowClass: Class<*>, windowData: Any? = null, title: String? = null, icon: ImageReference? = null) {
        showWindow(windowClass, windowData?.javaClass, windowData, title, icon)
    }

    override fun showWindow(windowClass: Class<*>, windowDataClass: Class<*>?, windowData: Any?, title: String?, icon: ImageReference?) {
        runLater {
            val window = component.find(windowClass as Class<out JavaFXWindow>,
                mapOf(JavaFXWindow::windowDataParam to windowData))

            window.show(title, (icon as? JavaFXImageReference)?.imageUrl)
        }
    }

}