package net.dankito.utils.windowregistry.window.javafx.ui.controls

import javafx.collections.FXCollections
import javafx.scene.layout.Priority
import net.dankito.utils.windowregistry.window.IWindow
import net.dankito.utils.windowregistry.window.WindowRegistry
import net.dankito.utils.windowregistry.window.WindowState
import net.dankito.utils.windowregistry.window.WindowsChangedListener
import tornadofx.*


open class OpenedWindowsView(protected val windowRegistry: WindowRegistry) : View() {

    private val openWindowsStates = FXCollections.observableArrayList<WindowState>()


    init {
        windowRegistry.addListener(createWindowsChangedListener())
    }


    override val root = vbox {
        label(messages["open.windows"]) {
            prefHeight = 26.0
        }

        listview<WindowState>(openWindowsStates) {
            cellFragment(OpenWindowListCellFragment::class)

            vboxConstraints {
                vGrow = Priority.ALWAYS
            }
        }
    }


    protected open fun createWindowsChangedListener(): WindowsChangedListener {
        return object : WindowsChangedListener {
            override fun windowsChanged(windows: List<IWindow>) {
                windowsChanged()
            }

        }
    }

    protected open fun windowsChanged() {
        openWindowsStates.setAll(windowRegistry.windowStates)
    }

}