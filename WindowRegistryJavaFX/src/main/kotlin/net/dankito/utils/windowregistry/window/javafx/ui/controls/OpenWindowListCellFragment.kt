package net.dankito.utils.windowregistry.window.javafx.ui.controls

import javafx.beans.property.SimpleStringProperty
import javafx.geometry.Pos
import javafx.scene.layout.Priority
import javafx.scene.paint.Color
import net.dankito.utils.javafx.ui.extensions.setBackgroundToColor
import net.dankito.utils.windowregistry.window.WindowState
import net.dankito.utils.windowregistry.window.javafx.ui.model.OpenWindowItemViewModel
import tornadofx.*


open class OpenWindowListCellFragment : ListCellFragment<WindowState>() {

    companion object {
        protected const val CellHeight = 100.0

        protected const val TitleLabelHeight = 20.0

        protected const val IconWidth = TitleLabelHeight
    }


    protected val openWindow = OpenWindowItemViewModel().bindTo(this)

    protected val tooltipText = SimpleStringProperty("")


    init {
        itemProperty.addListener { _, _, newValue ->
            tooltipText.set(if (newValue == null) "" else openWindow.title.value)
        }
    }

    override val root = stackpane {
        cellProperty.addListener { _, _, newValue -> // so that it always has cell's width
            newValue?.let { prefWidthProperty().bind(it.widthProperty().subtract(16)) }
        }

        alignment = Pos.CENTER
        minHeight = CellHeight
        prefHeight = CellHeight

        imageview(openWindow.screenshotUrl) {
            prefHeight = CellHeight
            cellProperty.addListener { _, _, newValue ->
                newValue?.let { prefWidthProperty().bind(it.widthProperty().subtract(16)) }
            }

            isPreserveRatio = true
            fitHeight = CellHeight
        }

        hbox {
            minHeight = TitleLabelHeight
            maxHeight = TitleLabelHeight
            useMaxWidth = true

            setBackgroundToColor(Color.SLATEGRAY)

            cellProperty.addListener { _, _, newValue ->
                newValue?.let {
                    this.minWidth = it.width - 16
                    this.prefWidthProperty().bind(it.widthProperty().subtract(16))
                }
            }

            stackpaneConstraints {
                alignment = Pos.BOTTOM_LEFT
                marginTop = CellHeight - TitleLabelHeight
                marginLeft = 0.0
                marginBottom = 0.0
                marginRight = 0.0
            }


            imageview(openWindow.icon) {
                maxHeight = TitleLabelHeight
                minWidth = IconWidth
                maxWidth = IconWidth

                fitHeight = TitleLabelHeight
                fitWidth = IconWidth
                isPreserveRatio = true
            }

            label(openWindow.title) {
                maxHeight = TitleLabelHeight

                hboxConstraints {
                    this.hGrow = Priority.ALWAYS
                    marginLeft = 6.0
                }
            }
        }

        tooltip {
            textProperty().bind(tooltipText)

            isWrapText = true

            maxWidth = 600.0
        }
    }

}