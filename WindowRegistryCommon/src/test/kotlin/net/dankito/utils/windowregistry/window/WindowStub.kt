package net.dankito.utils.windowregistry.window

import java.io.File


class WindowStub(private val windowTitle: String = "", private val data: Any? = null) : IWindow {

    private val listeners = LinkedHashSet<WindowListener>()


    override var windowData = data

    override val displayText = windowTitle

    override val windowDataClass = null

    override val isMainWindow = false


    override val windowIcon = null

    override val screenX = 0.0

    override val screenY = 0.0

    override val width = 0.0

    override val height = 0.0


    override fun getCurrentWindowData(): Any? {
        return windowData
    }

    override fun saveScreenshotTo(screenshotFile: File): File? {
        return null
    }



    override fun addWindowListener(listener: WindowListener) {
        listeners.add(listener)
    }

    override fun removeWindowListener(listener: WindowListener) {
        listeners.remove(listener)
    }

    fun callWindowCameToForegroundListeners() {
        ArrayList(listeners).forEach { listener ->
            listener.windowCameToForeground()
        }
    }

    fun callWindowGoesToBackgroundListeners() {
        ArrayList(listeners).forEach { listener ->
            listener.windowGoesToBackground()
        }
    }

    fun callWindowClosesListeners() {
        ArrayList(listeners).forEach { listener ->
            listener.windowCloses()
        }
    }

}