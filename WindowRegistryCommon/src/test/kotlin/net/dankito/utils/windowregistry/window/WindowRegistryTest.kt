package net.dankito.utils.windowregistry.window

import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.eq
import com.nhaarman.mockito_kotlin.verify
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.times
import org.mockito.junit.MockitoJUnitRunner


@RunWith(MockitoJUnitRunner::class)
class WindowRegistryTest {

    companion object {
        private const val FirstWindowTitle = "1"
        private const val FirstWindowData = "TestData"

        private const val SecondWindowTitle = "2"
    }


    @Mock
    private lateinit var windowStatePersister: IWindowStatePersister

    private lateinit var underTest: WindowRegistry


    @Before
    fun setUp() {
        underTest = WindowRegistry(windowStatePersister)
    }


    @Test
    fun createWindow_WindowsGetsUpdated() {

        // when
        val firstWindow = createWindow(FirstWindowTitle)
        underTest.windowCreated(firstWindow)

        // then
        assertThat(underTest.windows).containsExactly(firstWindow)
    }

    @Test
    fun createWindow_WindowStatesGetsUpdated() {

        // when
        underTest.windowCreated(createWindow(FirstWindowTitle))

        // then
        assertThat(underTest.windowStates).hasSize(1)

        assertThat(underTest.windowStates[0].index).isEqualTo(0)
        assertThat(underTest.windowStates[0].title).isEqualTo(FirstWindowTitle)
    }


    @Test
    fun createSecondWindow_WindowsGetsUpdated() {

        // given
        val firstWindow = createWindow(FirstWindowTitle)
        underTest.windowCreated(firstWindow)


        // when
        val secondWindow = createWindow(SecondWindowTitle)
        underTest.windowCreated(secondWindow)


        // then
        assertThat(underTest.windows).containsExactly(secondWindow, firstWindow)
    }

    @Test
    fun createSecondWindow_WindowStatesGetsUpdated() {

        // given
        underTest.windowCreated(createWindow(FirstWindowTitle))


        // when
        underTest.windowCreated(createWindow(SecondWindowTitle))


        // then
        assertThat(underTest.windowStates).hasSize(2)

        assertThat(underTest.windowStates[0].index).isEqualTo(0)
        assertThat(underTest.windowStates[0].title).isEqualTo(SecondWindowTitle)

        assertThat(underTest.windowStates[1].index).isEqualTo(1)
        assertThat(underTest.windowStates[1].title).isEqualTo(FirstWindowTitle)
    }


    @Test
    fun activateFirstWindow_WindowsGetsUpdated() {

        // given
        val firstWindow = createWindow(FirstWindowTitle)
        underTest.windowCreated(firstWindow)

        val secondWindow = createWindow(SecondWindowTitle)
        underTest.windowCreated(secondWindow)


        // when
        firstWindow.callWindowCameToForegroundListeners()


        // then
        assertThat(underTest.windows).containsExactly(firstWindow, secondWindow)
    }

    @Test
    fun activateFirstWindow_WindowStatesGetsUpdated() {

        // given
        val firstWindow = createWindow(FirstWindowTitle)
        underTest.windowCreated(firstWindow)

        underTest.windowCreated(createWindow(SecondWindowTitle))


        // when
        firstWindow.callWindowCameToForegroundListeners()


        // then
        assertThat(underTest.windowStates).hasSize(2)

        assertThat(underTest.windowStates[0].index).isEqualTo(0)
        assertThat(underTest.windowStates[0].title).isEqualTo(FirstWindowTitle)

        assertThat(underTest.windowStates[1].index).isEqualTo(1)
        assertThat(underTest.windowStates[1].title).isEqualTo(SecondWindowTitle)
    }


    @Test
    fun deactivateFirstWindow_StateGetsPersisted() {

        // given
        val firstWindow = createWindow(FirstWindowTitle, FirstWindowData)
        underTest.windowCreated(firstWindow)


        // when
        firstWindow.callWindowGoesToBackgroundListeners()


        // then
        verify(windowStatePersister, times(2)).persistWindowData(any())

        verify(windowStatePersister, times(2)).persistWindowStates(any(), eq(false))
    }


    @Test
    fun closeFirstWindow_WindowsGetsUpdated() {

        // given
        val firstWindow = createWindow(FirstWindowTitle)
        underTest.windowCreated(firstWindow)

        val secondWindow = createWindow(SecondWindowTitle)
        underTest.windowCreated(secondWindow)


        // when
        firstWindow.callWindowClosesListeners()


        // then
        assertThat(underTest.windows).containsExactly(secondWindow)
    }

    @Test
    fun closeFirstWindow_WindowStatesGetsUpdated() {

        // given
        val firstWindow = createWindow(FirstWindowTitle)
        underTest.windowCreated(firstWindow)

        underTest.windowCreated(createWindow(SecondWindowTitle))


        // when
        firstWindow.callWindowClosesListeners()


        // then
        assertThat(underTest.windowStates).hasSize(1)

        assertThat(underTest.windowStates[0].index).isEqualTo(0)
        assertThat(underTest.windowStates[0].title).isEqualTo(SecondWindowTitle)
    }


    @Test
    fun closeSecondWindow_WindowsGetsUpdated() {

        // given
        val firstWindow = createWindow(FirstWindowTitle)
        underTest.windowCreated(firstWindow)

        val secondWindow = createWindow(SecondWindowTitle)
        underTest.windowCreated(secondWindow)


        // when
        secondWindow.callWindowClosesListeners()


        // then
        assertThat(underTest.windows).containsExactly(firstWindow)
    }

    @Test
    fun closeSecondWindow_WindowStatesGetsUpdated() {

        // given
        val firstWindow = createWindow(FirstWindowTitle)
        underTest.windowCreated(firstWindow)

        val secondWindow = createWindow(SecondWindowTitle)
        underTest.windowCreated(secondWindow)


        // when
        secondWindow.callWindowClosesListeners()


        // then
        assertThat(underTest.windowStates).hasSize(1)

        assertThat(underTest.windowStates[0].index).isEqualTo(0)
        assertThat(underTest.windowStates[0].title).isEqualTo(FirstWindowTitle)
    }


    private fun createWindow(title: String, windowData: Any? = null): WindowStub {
        return WindowStub(title, windowData)
    }

}