package net.dankito.utils.windowregistry.window


interface WindowsChangedListener {

    fun windowsChanged(windows: List<IWindow>)

}