package net.dankito.utils.windowregistry.ui.router

import net.dankito.utils.image.ImageReference


interface IRouter {

    fun showWindow(windowClass: Class<*>, windowStateId: String?)

    fun showWindow(windowClass: Class<*>, windowDataClass: Class<*>?, windowData: Any? = null, title: String? = null, icon: ImageReference? = null)

}