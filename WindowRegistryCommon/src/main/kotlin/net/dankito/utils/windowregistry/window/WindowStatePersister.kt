package net.dankito.utils.windowregistry.window

import net.dankito.utils.io.IFileStorageService
import net.dankito.utils.serialization.ISerializer
import net.dankito.utils.serialization.JacksonJsonSerializer
import org.slf4j.LoggerFactory
import java.io.File


open class WindowStatePersister(protected val fileStorageService: IFileStorageService,
                                protected val serializer: ISerializer = JacksonJsonSerializer()) : IWindowStatePersister {

    companion object {
        const val OpenedWindowsFolderName = "opened_windows"

        const val WindowStatesFileName = "window_states.json"

        private val log = LoggerFactory.getLogger(WindowStatePersister::class.java)
    }


    override fun persistWindowStates(windowStates: List<WindowState>, persistWindowData: Boolean) {
        log.info("persistWindowStates(${windowStates.size}, $persistWindowData) called")

        if (persistWindowData) {
            windowStates.forEach { state ->
                persistWindowData(state)
            }
        }

        writeToDataFolder(WindowStatesFileName, windowStates)
    }

    override fun persistWindowData(state: WindowState) {
        state.windowData?.let { windowData ->
            state.serializedWindowDataPath = writeToDataFolder("${state.id}.json", windowData)
        }
    }

    protected open fun writeToDataFolder(filename: String, data: Any): File {
        val dataJson = serializer.serializeObject(data)

        val outputFile = fileStorageService.getFileInDataFolder(filename, OpenedWindowsFolderName)

        fileStorageService.writeToTextFile(dataJson, outputFile)

        return outputFile
    }


    override fun restoreWindowStatesWithoutWindowData(): List<WindowState> {
        val windowStatesFile = fileStorageService.getFileInDataFolder(WindowStatesFileName, OpenedWindowsFolderName)

        if (windowStatesFile.exists()) {
            try {
                fileStorageService.readFromTextFile(windowStatesFile)?.let { windowStatesJson ->
                    return serializer.deserializeObject(windowStatesJson, List::class.java, WindowState::class.java) as List<WindowState>
                }
            } catch (e: Exception) { log.error("Could not restore window states", e) }
        }

        return listOf()
    }

    override fun restoreWindowData(state: WindowState): Any? {
        state.windowDataClass?.let { windowDataClassName ->
            state.serializedWindowDataPath?.let { windowDataPath ->
                try {
                    val windowDataClass = Class.forName(windowDataClassName)

                    return restoreWindowData(windowDataClass, windowDataPath)
                } catch (e: Exception) {
                    log.error("Could not restore window data for state $state", e)
                }
            }
        }

        return null
    }

    @Throws(Exception::class)
    override fun restoreWindowData(windowDataClass: Class<*>, windowDataPath: File): Any? {
        fileStorageService.readFromTextFile(windowDataPath)?.let { windowDataJson ->
            return serializer.deserializeObject(windowDataJson, windowDataClass)
        }

        return null
    }


    override fun getScreenshotFile(state: WindowState): File {
        return fileStorageService.getFileInDataFolder("${state.id}.png", OpenedWindowsFolderName)
    }

}