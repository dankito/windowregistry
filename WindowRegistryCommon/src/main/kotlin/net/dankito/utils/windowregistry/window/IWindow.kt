package net.dankito.utils.windowregistry.window

import net.dankito.utils.ui.image.Image
import java.io.File


interface IWindow {

    var windowData: Any?

    val displayText: CharSequence

    val windowIcon: Image?

    val windowClass: String
        get() = this::class.java.name

    val windowClassName: String
        get() = this::class.java.simpleName

    val windowDataClass: Class<*>?

    val isMainWindow: Boolean


    val screenX: Double

    val screenY: Double

    val width: Double

    val height: Double


    fun getCurrentWindowData(): Any?


    fun saveScreenshotTo(screenshotFile: File): File?


    fun addWindowListener(listener: WindowListener)

    fun removeWindowListener(listener: WindowListener)

}