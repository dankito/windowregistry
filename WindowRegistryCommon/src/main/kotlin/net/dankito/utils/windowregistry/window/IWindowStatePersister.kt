package net.dankito.utils.windowregistry.window

import java.io.File


interface IWindowStatePersister {

    fun persistWindowStates(windowStates: List<WindowState>, persistWindowData: Boolean)

    fun persistWindowData(state: WindowState)


    fun restoreWindowStatesWithoutWindowData(): List<WindowState>

    fun restoreWindowData(state: WindowState): Any?

    @Throws(Exception::class)
    fun restoreWindowData(windowDataClass: Class<*>, windowDataPath: File): Any?


    fun getScreenshotFile(state: WindowState): File

}