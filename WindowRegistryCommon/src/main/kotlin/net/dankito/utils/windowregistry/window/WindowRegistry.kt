package net.dankito.utils.windowregistry.window

import net.dankito.utils.windowregistry.ui.router.IRouter
import org.slf4j.LoggerFactory
import java.util.*
import java.util.concurrent.CopyOnWriteArrayList
import kotlin.concurrent.schedule
import kotlin.concurrent.thread


open class WindowRegistry(protected val windowStatePersister: IWindowStatePersister) {

    companion object {
        private val log = LoggerFactory.getLogger(WindowRegistry::class.java)
    }


    protected val openedWindows = ArrayList<IWindow>()

    protected val openedWindowsStates = HashMap<IWindow, WindowState>()

    protected val listeners = LinkedHashSet<WindowsChangedListener>()

    protected val nextWindowCreatedListeners = CopyOnWriteArrayList<(IWindow) -> Unit>()

    protected var restoredWindowStates = false

    protected var hasMainWindowBeenClosed = false


    open var currentWindow: IWindow? = null
        protected set

    open val windows: List<IWindow>
        get() = ArrayList(openedWindows)

    open val windowStates: List<WindowState>
        get() = ArrayList(openedWindowsStates.values.sortedBy { it.index })


    fun windowCreated(window: IWindow, windowData: Any? = null, windowStateId: String? = null): WindowState {
        log.info("windowCreated $window")

        currentWindow = window

        window.addWindowListener(createWindowListener(window))

        val windowState = getOrCreateAndSaveWindowState(window, windowStateId, windowData)

        openedWindowsStates.put(window, windowState)

        updateIndexForNewForegroundWindow(window) // TODO: needed here or is it sufficient to call it in windowActivated()?

        saveWindowStates()

        callWindowsChangedListeners()
        callAndClearNextWindowCreatedListeners(window)

        return windowState
    }

    protected open fun windowActivated(window: IWindow) {
        log.info("windowActivated $window")

        if (currentWindow != window) {
            currentWindow = window
        }

//        getState(window)?.apply {
//            title = window.displayText.toString()
//            icon = window.windowIcon
//        }

        updateIndexForNewForegroundWindow(window)

//        if (restoredWindowStates) {
//            saveWindowStates()
//        }

        callWindowsChangedListeners()
    }

    protected open fun windowDeactivated(window: IWindow) {
        log.info("windowDeactivated $window")

        getState(window)?.let { windowState ->
            updateWindowState(window, windowState)

            saveWindowData(windowState)

            saveWindowStates()
        }
    }

    protected open fun windowCloses(window: IWindow, listener: WindowListener) {
        log.info("windowCloses $window")

        if (currentWindow == window) {
            currentWindow = null
        }

        // to avoid that after main window has been closed closing other windows corrupts window states
        if (hasMainWindowBeenClosed == false) {
            if (window.isMainWindow == false) {
                getState(window)?.let { state ->
                    state.serializedWindowDataPath?.delete()

                    state.screenshotFile?.delete()
                }

                openedWindows.remove(window)

                openedWindowsStates.remove(window)

                updateIndices()
            }
            else {
                hasMainWindowBeenClosed = true
            }

            saveWindowStates()
        }

        window.removeWindowListener(listener)

        callWindowsChangedListeners()
    }

    open fun applicationCloses() {
        updateCurrentWindowWindowState()

        saveWindowStates()
    }


    protected open fun updateIndexForNewForegroundWindow(window: IWindow) {
        // move window to first position in Set
        openedWindows.remove(window)

        openedWindows.add(0, window)

        updateIndices()
    }

    open fun updateCurrentWindowWindowState() {
        currentWindow?.let { currentWindow ->
            updateWindowState(currentWindow)
        }
    }

    protected open fun updateWindowState(window: IWindow) {
        getState(window)?.let { windowState ->
            updateWindowState(window, windowState)
        }
    }

    protected open fun updateWindowState(window: IWindow, windowState: WindowState) {
        windowState.title = window.displayText.toString()

        windowState.icon = window.windowIcon

        windowState.screenX = window.screenX
        windowState.screenY = window.screenY
        windowState.width = window.width
        windowState.height = window.height

        windowState.windowData = window.getCurrentWindowData()

        try {
            window.saveScreenshotTo(windowStatePersister.getScreenshotFile(windowState))?.let { screenshotFile ->
                windowState.screenshotFile = screenshotFile
            }
        } catch (e: Exception) {
            log.warn("Could not take screenshot for window $windowState", e)
        }
    }


    protected open fun getOrCreateAndSaveWindowState(window: IWindow, windowStateId: String?, windowData: Any?): WindowState {
        val virtualWindowAndState = getStateForId(windowStateId)

        if (virtualWindowAndState != null) {
            log.info("Removing VirtualWindow ${virtualWindowAndState.second}")

            openedWindowsStates.remove(virtualWindowAndState.first)
            openedWindows.remove(virtualWindowAndState.first)

            virtualWindowAndState.second.icon = window.windowIcon

            return virtualWindowAndState.second
        }
        else {
            return createAndSaveWindowState(window, windowData)
        }
    }

    protected open fun createAndSaveWindowState(window: IWindow, windowData: Any?): WindowState {
        val windowState = createWindowState(window.windowClass, window.windowDataClass, window.windowData, window.displayText.toString())

        windowState.icon = window.windowIcon

        windowState.screenX = window.screenX
        windowState.screenY = window.screenY
        windowState.width = window.width
        windowState.height = window.height

        saveWindowData(windowState)

        return windowState
    }

    protected open fun createAndSaveWindowState(windowClass: String, windowDataClass: Class<*>?, windowData: Any?, title: String?): WindowState {
        val windowState = createWindowState(windowClass, windowDataClass, windowData, title)

        saveWindowData(windowState)

        return windowState
    }

    protected open fun createWindowState(windowClass: String, windowDataClass: Class<*>?, windowData: Any?, title: String?): WindowState {
        val stateId = "${windowClass}_${UUID.randomUUID()}"

        return WindowState(
            stateId, windowClass, 0, title ?: "",
            windowDataClass?.name, null, windowData
        )
    }


    open fun getWindowDataForStateId(windowStateId: String?): Any? {
        return getStateForId(windowStateId)?.second?.windowData
    }

    protected open fun getStateForId(windowStateId: String?): Pair<IWindow, WindowState>? {
        if (windowStateId != null) {
            openedWindowsStates.entries.forEach { entry ->
                if (entry.value.id == windowStateId) {
                    return Pair(entry.key, entry.value)
                }
            }
        }

        return null
    }

    protected open fun getState(window: IWindow) = openedWindowsStates[window]


    protected open fun updateIndices() {
        openedWindows.forEachIndexed { index, window ->
            getState(window)?.index = index
        }
    }


    open fun restoreWindowStates(mainWindow: IWindow, router: IRouter): WindowState? {
        val restoredStates = windowStatePersister.restoreWindowStatesWithoutWindowData().toMutableList()

        log.info("Restored window states:")
        restoredStates.forEach { log.info(it.toString()) }

        val mainWindowClassName = mainWindow.javaClass.name

        val mainWindowState = restoreMainWindowState(restoredStates, mainWindow, mainWindowClassName)

        thread {
            restoreWindows(restoredStates, router, mainWindow, mainWindowClassName)

            this.restoredWindowStates = true
        }

        return mainWindowState
    }

    protected open fun restoreMainWindowState(restoredStates: MutableList<WindowState>, mainWindow: IWindow,
                                         mainWindowClassName: String?): WindowState? {

        restoredStates.filter { it.windowClass == mainWindowClassName }.firstOrNull()?.let { mainWindowState ->
            openedWindowsStates.put(mainWindow, mainWindowState) // TODO: i think that's not needed anymore, also for MainWindow windowCreated() gets called

            val mainWindowData = windowStatePersister.restoreWindowData(mainWindowState)

            mainWindowState.windowData = mainWindowData

            restoredStates.remove(mainWindowState)

            return mainWindowState
        }

        return null
    }

    protected open fun restoreWindows(states: List<WindowState>, router: IRouter, mainWindow: IWindow, mainWindowClassName: String) {
        states.sortedByDescending { it.index }.forEach { restoreWindow(it, router, mainWindow, mainWindowClassName) }
    }

    protected open fun restoreWindow(state: WindowState, router: IRouter, mainWindow: IWindow, mainWindowClassName: String) {
        try {
            state.windowData = windowStatePersister.restoreWindowData(state)

            val windowClass = Class.forName(state.windowClass) as Class<IWindow>

            if (state.windowClass == mainWindowClassName) {
//                mainWindow.restoreWindowState(state) // TODO: how to call restoreWindowState() on UI thread?
            }
            else {
                registerVirtualWindow(state)

                router.showWindow(windowClass, state.id)
            }
        } catch (e: Exception) {
            log.error("Could not restoreWindow $state", e)
        }
    }


    protected open fun saveWindowData(state: WindowState) {
        state.windowData?.let { windowData ->
            windowStatePersister.persistWindowData(state)
        }
    }

    protected open fun saveWindowStates() {
        windowStatePersister.persistWindowStates(windowStates, false)
    }


    fun goingToCreateNewWindow(windowClass: Class<*>, windowDataClass: Class<*>?, windowData: Any?, title: String?): String {
        val windowState = createAndSaveWindowState(windowClass.name, windowDataClass, windowData, title)

        registerVirtualWindow(windowState)

        return windowState.id
    }

    protected open fun registerVirtualWindow(state: WindowState) {
        registerVirtualWindow(VirtualWindow(state), state)
    }

    protected open fun registerVirtualWindow(window: VirtualWindow, state: WindowState) {
        if (openedWindows.isNotEmpty()
            && openedWindows[0] is VirtualWindow == false
            && state.windowClass == getState(openedWindows[0])?.windowClass) {
            openedWindows.add(1, window) // add after currently displayed window
        }
        else {
            openedWindows.add(0, window) // on restore
        }

        openedWindowsStates.put(window, state)

        updateIndices()

        saveWindowStates()

        log.info("Registered virtual window $state")
    }


    protected open fun createWindowListener(window: IWindow): WindowListener {
        return object : WindowListener {
            override fun windowCameToForeground() {
                windowActivated(window)
            }

            override fun windowGoesToBackground() {
                windowDeactivated(window)
            }

            override fun windowCloses() {
                windowCloses(window, this)
            }

        }
    }


    open fun addListener(listener: WindowsChangedListener) {
        listeners.add(listener)
    }

    open fun removeListener(listener: WindowsChangedListener) {
        listeners.remove(listener)
    }

    protected open fun callWindowsChangedListeners() {
        val copy = ArrayList(openedWindows)

        ArrayList(listeners).forEach { listener ->
            listener.windowsChanged(copy)
        }
    }

    fun addNextWindowCreatedListener(listener: (IWindow) -> Unit) {
        synchronized(nextWindowCreatedListeners) {
            nextWindowCreatedListeners.add(listener)
        }
    }

    private fun callAndClearNextWindowCreatedListeners(window: IWindow) {
        synchronized(nextWindowCreatedListeners) {
            val listenersCopy = ArrayList(nextWindowCreatedListeners)

            nextWindowCreatedListeners.clear()

            Timer().schedule(500) { // wait some time till activity is initialized
                listenersCopy.forEach { it(window) }
            }
        }
    }

}