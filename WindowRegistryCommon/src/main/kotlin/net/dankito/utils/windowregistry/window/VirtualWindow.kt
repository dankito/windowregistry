package net.dankito.utils.windowregistry.window

import java.io.File


/**
 * After restoring window states on app start it takes some time till real window gets created.
 * So till real window gets created we add a 'virtual' window with its restored state to not loose its restored state.
 *
 * It's also useful on Android as not each call to Context.startActivity() really starts an activity, e.g. if there are
 * already too many Activities on screen.
 * So with help of this class we can keep track of this yet undisplayed activities and may even switch to it.
 */
open class VirtualWindow(protected val state: WindowState) : IWindow {

    override var windowData: Any? = state.windowData

    override val displayText = state.title

    override val windowClass = state.windowClass

    override val windowClassName = state.windowClassName

    override val windowDataClass: Class<*>?
        get() {
            try {
                return Class.forName(state.windowDataClass)
            } catch (ignored: Exception) { }

            return null
        }

    override val isMainWindow = false


    override val windowIcon = state.icon

    override val screenX = state.screenX

    override val screenY = state.screenY

    override val width = state.width

    override val height = state.height


    override fun getCurrentWindowData(): Any? {
        return windowData
    }

    override fun saveScreenshotTo(screenshotFile: File): File? {
        return state.screenshotFile
    }

    override fun addWindowListener(listener: WindowListener) {
    }

    override fun removeWindowListener(listener: WindowListener) {
    }

}