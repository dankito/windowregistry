package net.dankito.utils.windowregistry.window


interface IWindowStateRestorer {

    fun restoreWindows(states: List<WindowState>)

    fun restoreWindow(state: WindowState)

}