package net.dankito.utils.windowregistry.window

import net.dankito.utils.ui.image.Image
import java.io.File


open class WindowState(val id: String,
                       val windowClass: String,
                       var index: Int,
                       var title: String,
                       val windowDataClass: String?,
                       var serializedWindowDataPath: File? = null,
                       @Transient var windowData: Any? = null,
                       var screenshotFile: File? = null,
                       @Transient var icon: Image? = null,
                       var screenX: Double = PositionUnset, // no use for these on Android
                       var screenY: Double = PositionUnset,
                       var width: Double = SizeUnset,
                       var height: Double = SizeUnset
) {

    companion object {

        const val UnsetIndex = Int.MAX_VALUE

        private val PositionUnset = Double.NaN

        private val SizeUnset = Double.NaN

    }

    private constructor() : this("","", UnsetIndex, "", null) // for Jackson


    open val isWindowPositionSet: Boolean
        get() = screenX.isNaN() == false && screenY.isNaN() == false

    open val isWindowSizeSet: Boolean
        get() = width.isNaN() == false && width > 0.0 && height.isNaN() == false && height > 0.0


    open val windowClassName: String
        get() {
            // + 1 as we don't want the dot get displayed in toString() and if no dot is found lastIndexOf() returns - 1
            // -> lastDotIndex can never be smaller 0 -> no IndexOutOfBoundException
            val lastDotIndex = windowClass.lastIndexOf('.') + 1

            return windowClass.substring(lastDotIndex)
        }


    override fun toString(): String {
        return "[$index] $windowClassName: $title"
    }

}