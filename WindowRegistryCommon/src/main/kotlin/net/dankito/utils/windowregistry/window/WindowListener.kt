package net.dankito.utils.windowregistry.window


interface WindowListener {

    fun windowCameToForeground()

    fun windowGoesToBackground()

    fun windowCloses()

}